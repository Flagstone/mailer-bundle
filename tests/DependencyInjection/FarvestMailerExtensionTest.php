<?php


namespace Farvest\MailerBundle\Tests\DependencyInjection;


use Farvest\MailerBundle\DependencyInjection\FlagstoneMailerExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Exception;

class FarvestMailerExtensionTest extends TestCase
{
    /**
     *  @var FlagstoneMailerExtension
     */
    private $extension;
    private $root;

    public function setUp(): void
    {
        $this->extension = new FlagstoneMailerExtension();
        $this->root = "farvest_mailer";
    }

    /**
     *  @throws Exception
     */
    public function testLoadParameters()
    {
        $this->extension->load(array(), $container = $this->getContainer());
        $this->assertTrue($container->hasParameter('sender'));
        $this->assertTrue($container->hasParameter('from'));
        $this->assertTrue($container->hasParameter('reply_to'));
        $sender = $container->getParameter('sender');
        // $this->assertTrue(array_key_exists('name', $sender));
        // $this->assertTrue(array_key_exists('email', $sender));
    }

    private function getContainer()
    {
        return new ContainerBuilder();
    }
}