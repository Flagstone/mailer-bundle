<?php
/** *****************************************************************************************************************
 *  MailAddressTest.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/23
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Tests\Entity;

use Farvest\MailerBundle\Entity\MailBody;
use PHPUnit\Framework\TestCase;
use TypeError;

/** *****************************************************************************************************************
 *  Class MailBodyTest
 *  -----------------------------------------------------------------------------------------------------------------
 *  Tests for the MailBody entity.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Tests\Entity
 *  ***************************************************************************************************************** */
class MailBodyTest extends TestCase
{
    const STR_TEST_BODY = '<div><p>This is the HTML Body test string</p></div>';
    const STR_TEST_TXT = 'This is the Txt Body test string';

    /** **************************************************************************************************************
     *  Test html assertion
     *  ************************************************************************************************************* */
    public function testGetHtml()
    {
        $mailBody = new MailBody();
        $mailBody->setHtml(self::STR_TEST_BODY);
        $this->assertIsString($mailBody->getHtml());
        $this->assertEquals(self::STR_TEST_BODY, $mailBody->getHtml());
    }

    /** **************************************************************************************************************
     *  Test txt assertion
     *  ************************************************************************************************************* */
    public function tesGetTxt()
    {
        $mailBody = new MailBody();
        $mailBody->setTxt(self::STR_TEST_TXT);
        $this->assertIsString($mailBody->getTxt());
        $this->assertEquals(self::STR_TEST_TXT, $mailBody->getTxt());
    }

    /** **************************************************************************************************************
     *  Test txt assertion can be null
     *  ************************************************************************************************************* */
    public function testGetTxtNull()
    {
        $mailBody = new MailBody();
        $mailBody->setTxt(null);
        $this->assertNull($mailBody->getTxt());
    }

    /** *************************************************************************************************************
     *  Test null html body assertion not accepted
     *  -------------------------------------------------------------------------------------------------------------
     *  @expectedException
     *  @test
     *  ************************************************************************************************************* */
    public function testGetHtmlNull()
    {
        $mailBody = new MailBody();
        $this->expectException(TypeError::class);
        $mailBody->setHtml(null);
    }
}