<?php
/** *****************************************************************************************************************
 *  MailAttachmentTest.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/23
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Tests\Entity;

use Farvest\MailerBundle\Entity\MailAttachment;
use PHPUnit\Framework\TestCase;

/** *****************************************************************************************************************
 *  Class MailAttachmentTest
 *  -----------------------------------------------------------------------------------------------------------------
 *  Tests for the MailAttachment entity.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Tests\Entity
 *  ***************************************************************************************************************** */
class MailAttachmentTest extends TestCase
{
    const STR_TEST_FROM_PATH = '/home/mytest/toto.jpeg';
    const STR_TEST_FILENAME = 'toto.jpeg';
    const STR_TEST_CONTENT_TYPE = 'image/jpeg';
    const STR_TEST_BODY = 'This is a content. This must be pdf, jpeg, ... string';

    /** **************************************************************************************************************
     *  Test fromPath assertion
     *  ************************************************************************************************************* */
    public function testGetFromPath()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setFromPath(self::STR_TEST_FROM_PATH);
        $this->assertIsString($mailAttachment->getFromPath());
        $this->assertEquals(self::STR_TEST_FROM_PATH, $mailAttachment->getFromPath());
    }

    /** **************************************************************************************************************
     *  Test filename assertion
     *  ************************************************************************************************************* */
    public function testGetFilename()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setFilename(self::STR_TEST_FILENAME);
        $this->assertIsString($mailAttachment->getFilename());
        $this->assertEquals(self::STR_TEST_FILENAME, $mailAttachment->getFilename());
    }

    /** **************************************************************************************************************
     *  Test contentType assertion
     *  ************************************************************************************************************* */
    public function testGetContentType()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setContentType(self::STR_TEST_CONTENT_TYPE);
        $this->assertIsString($mailAttachment->getContentType());
        $this->assertEquals(self::STR_TEST_CONTENT_TYPE, $mailAttachment->getContentType());
    }

    /** **************************************************************************************************************
     *  Test body assertion
     *  ************************************************************************************************************* */
    public function testGetBody()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setBody(self::STR_TEST_BODY);
        $this->assertIsString($mailAttachment->getBody());
        $this->assertEquals(self::STR_TEST_BODY, $mailAttachment->getBody());
    }

    /** **************************************************************************************************************
     *  Test fromPath assertion can be null
     *  ************************************************************************************************************* */
    public function testGetFromPathNull()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setFromPath(null);
        $this->assertNull($mailAttachment->getFromPath());
    }

    /** **************************************************************************************************************
     *  Test fromPath assertion can be null
     *  ************************************************************************************************************* */
    public function testGetFilenameNull()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setFilename(null);
        $this->assertNull($mailAttachment->getFilename());
    }

    /** **************************************************************************************************************
     *  Test fromPath assertion can be null
     *  ************************************************************************************************************* */
    public function testGetContentTypeNull()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setContentType(null);
        $this->assertNull($mailAttachment->getContentType());
    }

    /** **************************************************************************************************************
     *  Test fromPath assertion can be null
     *  ************************************************************************************************************* */
    public function testGetBodyNull()
    {
        $mailAttachment = new MailAttachment();
        $mailAttachment->setBody(null);
        $this->assertNull($mailAttachment->getBody());
    }
}