<?php
/** *****************************************************************************************************************
 *  MailAddressTest.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/23
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Tests\Entity;

use Farvest\MailerBundle\Entity\MailAddress;
use PHPUnit\Framework\TestCase;
use TypeError;

/** *****************************************************************************************************************
 *  Class MailAddressTest
 *  -----------------------------------------------------------------------------------------------------------------
 *  Tests for the MailAdress entity.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Tests\Entity
 *  ***************************************************************************************************************** */
class MailAddressTest extends TestCase
{
    const STR_TEST_NAME = 'my name';
    const STR_TEST_EMAIL = 'myname@myemail.com';

    const ATTR_STRING = 'string';

    /** **************************************************************************************************************
     *  Test name assertion
     *  ************************************************************************************************************* */
    public function testGetName()
    {
        $mailAddress = new MailAddress();
        $mailAddress->setName(self::STR_TEST_NAME);
        $this->assertIsString($mailAddress->getName());
        $this->assertEquals(self::STR_TEST_NAME, $mailAddress->getName());
    }

    /** **************************************************************************************************************
     *  Test email assertion
     *  ************************************************************************************************************* */
    public function testGetEmail()
    {
        $mailAddress = new MailAddress();
        $mailAddress->setEmail(self::STR_TEST_EMAIL);
        $this->assertIsString(self::ATTR_STRING, $mailAddress->getEmail());
        $this->assertEquals(self::STR_TEST_EMAIL, $mailAddress->getEmail());
    }

    /** **************************************************************************************************************
     *  Test null name assertion accepted
     *  ************************************************************************************************************* */
    public function testGetNameNull()
    {
        $mailAddress = new MailAddress();
        $mailAddress->setName(null);
        $this->assertNull($mailAddress->getName());

    }

    /** *************************************************************************************************************
     *  Test null email assertion not accepted
     *  -------------------------------------------------------------------------------------------------------------
     *  @expectedException
     *  @test
     *  ************************************************************************************************************* */
    public function testGetEmailNull()
    {
        $mailAddress = new MailAddress();
        $this->expectException(TypeError::class);
        $mailAddress->setEmail(null);
    }
}
