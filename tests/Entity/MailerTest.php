<?php
/** *****************************************************************************************************************
 *  MailerTest.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/23
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Tests\Entity;

use Farvest\MailerBundle\Entity\MailAddress;
use Farvest\MailerBundle\Entity\MailAttachment;
use Farvest\MailerBundle\Entity\MailBody;
use Farvest\MailerBundle\Entity\Mailer;
use PHPUnit\Framework\TestCase;
use TypeError;

/** *****************************************************************************************************************
 *  Class MailerTest
 *  -----------------------------------------------------------------------------------------------------------------
 *  Tests for the Mailer entity.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Tests\Entity
 *  ***************************************************************************************************************** */
class MailerTest extends TestCase
{
    const STR_TEST_SUBJECT = 'Subject for test Mailer';

    const ATTR_MAIL_ADDRESS_CLASS = MailAddress::class;
    const ATTR_MAIL_ATTACHMENT_CLASS = MailAttachment::class;
    const ATTR_MAIL_BODY_CLASS = MailBody::class;

    const ATTR_NB_ITER = 3;

    /** **************************************************************************************************************
     *  Test fromPath assertion
     *  ************************************************************************************************************* */
    public function testGetSender()
    {
        $mailer = new Mailer();
        $sender = new MailAddress();
        $sender
            ->setName(MailAddressTest::STR_TEST_NAME)
            ->setEmail(MailAddressTest::STR_TEST_EMAIL);

        $mailer->setSender($sender);
        $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $mailer->getSender());
        $this->assertEquals(MailAddressTest::STR_TEST_NAME, $mailer->getSender()->getName());
        $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $mailer->getSender()->getEmail());
    }

    /** **************************************************************************************************************
     *  Test fromPath assertion
     *  ************************************************************************************************************* */
    public function testGetTo()
    {
        $mailer = new Mailer();
        $to = new MailAddress();
        $to
            ->setName(MailAddressTest::STR_TEST_NAME)
            ->setEmail(MailAddressTest::STR_TEST_EMAIL);

        $mailer->setTo($to);
        $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $mailer->getTo());
        $this->assertEquals(MailAddressTest::STR_TEST_NAME, $mailer->getTo()->getName());
        $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $mailer->getTo()->getEmail());
    }

    /** **************************************************************************************************************
     *  Test from assertion
     *  ************************************************************************************************************* */
    public function testGetFrom()
    {
        $mailer = new Mailer();
        $from = new MailAddress();
        $from
            ->setName(MailAddressTest::STR_TEST_NAME)
            ->setEmail(MailAddressTest::STR_TEST_EMAIL);

        $mailer->setFrom($from);
        $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $mailer->getFrom());
        $this->assertEquals(MailAddressTest::STR_TEST_NAME, $mailer->getFrom()->getName());
        $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $mailer->getFrom()->getEmail());
    }

    /** **************************************************************************************************************
     *  Test replyTo assertion
     *  ************************************************************************************************************* */
    public function testGetReplyTo()
    {
        $mailer = new Mailer();
        $replyTo = new MailAddress();
        $replyTo
            ->setName(MailAddressTest::STR_TEST_NAME)
            ->setEmail(MailAddressTest::STR_TEST_EMAIL);

        $mailer->setReplyTo($replyTo);
        $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $mailer->getReplyTo());
        $this->assertEquals(MailAddressTest::STR_TEST_NAME, $mailer->getReplyTo()->getName());
        $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $mailer->getReplyTo()->getEmail());
    }

    /** **************************************************************************************************************
     *  Test cc assertion
     *  ************************************************************************************************************* */
    public function testAddCc()
    {
        $mailer = new Mailer();

        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $address = new MailAddress();
            $address
                ->setName(MailAddressTest::STR_TEST_NAME)
                ->setEmail(MailAddressTest::STR_TEST_EMAIL);
            $mailer->addCc($address);
        }

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getCc()));
        $this->assertIsArray($mailer->getCc());
        foreach ($mailer->getCc() as $cc) {
            $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $cc);
            $this->assertEquals(MailAddressTest::STR_TEST_NAME, $cc->getName());
            $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $cc->getEmail());
        }
    }

    /** **************************************************************************************************************
     *  Test cc assertion
     *  ************************************************************************************************************* */
    public function testSetCc()
    {
        $mailer = new Mailer();

        $cc = [];
        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $address = new MailAddress();
            $address
                ->setName(MailAddressTest::STR_TEST_NAME)
                ->setEmail(MailAddressTest::STR_TEST_EMAIL);
            $cc[] = $address;
        }
        $mailer->setCc($cc);

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getCc()));
        $this->assertIsArray($mailer->getCc());
        foreach ($mailer->getCc() as $cc) {
            $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $cc);
            $this->assertEquals(MailAddressTest::STR_TEST_NAME, $cc->getName());
            $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $cc->getEmail());
        }
    }

    /** **************************************************************************************************************
     *  Test bcc assertion
     *  ************************************************************************************************************* */
    public function testAddBcc()
    {
        $mailer = new Mailer();

        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $address = new MailAddress();
            $address
                ->setName(MailAddressTest::STR_TEST_NAME)
                ->setEmail(MailAddressTest::STR_TEST_EMAIL);
            $mailer->addBcc($address);
        }

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getBcc()));
        $this->assertIsArray($mailer->getBcc());
        foreach ($mailer->getBcc() as $bcc) {
            $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $bcc);
            $this->assertEquals(MailAddressTest::STR_TEST_NAME, $bcc->getName());
            $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $bcc->getEmail());
        }
    }

    /** **************************************************************************************************************
     *  Test cc assertion
     *  ************************************************************************************************************* */
    public function testSetBcc()
    {
        $mailer = new Mailer();

        $bcc = [];
        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $address = new MailAddress();
            $address
                ->setName(MailAddressTest::STR_TEST_NAME)
                ->setEmail(MailAddressTest::STR_TEST_EMAIL);
            $bcc[] = $address;
        }
        $mailer->setBcc($bcc);

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getBcc()));
        $this->assertIsArray($mailer->getBcc());
        foreach ($mailer->getBcc() as $bcc) {
            $this->assertInstanceOf(self::ATTR_MAIL_ADDRESS_CLASS, $bcc);
            $this->assertEquals(MailAddressTest::STR_TEST_NAME, $bcc->getName());
            $this->assertEquals(MailAddressTest::STR_TEST_EMAIL, $bcc->getEmail());
        }
    }

    /** **************************************************************************************************************
     *  Test attachment assertion
     *  ************************************************************************************************************* */
    public function testAddAttachment()
    {
        $mailer = new Mailer();

        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $attachment = new MailAttachment();
            $attachment
                ->setFromPath(MailAttachmentTest::STR_TEST_FROM_PATH)
                ->setFilename(MailAttachmentTest::STR_TEST_FILENAME)
                ->setContentType(MailAttachmentTest::STR_TEST_CONTENT_TYPE)
                ->setBody(MailAttachmentTest::STR_TEST_BODY);
            $mailer->addAttachment($attachment);
        }

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getAttachments()));
        $this->assertIsArray($mailer->getAttachments());
        foreach ($mailer->getAttachments() as $attachment) {
            $this->assertInstanceOf(self::ATTR_MAIL_ATTACHMENT_CLASS, $attachment);
            $this->assertEquals(MailAttachmentTest::STR_TEST_FROM_PATH, $attachment->getFromPath());
            $this->assertEquals(MailAttachmentTest::STR_TEST_FILENAME, $attachment->getFilename());
            $this->assertEquals(MailAttachmentTest::STR_TEST_CONTENT_TYPE, $attachment->getContentType());
            $this->assertEquals(MailAttachmentTest::STR_TEST_BODY, $attachment->getBody());
        }
    }

    /** **************************************************************************************************************
     *  Test attachment assertion
     *  ************************************************************************************************************* */
    public function testSetAttachment()
    {
        $mailer = new Mailer();

        $attachments = [];
        for ($iter = 0; $iter < self::ATTR_NB_ITER; $iter++) {
            $attachment = new MailAttachment();
            $attachment
                ->setFromPath(MailAttachmentTest::STR_TEST_FROM_PATH)
                ->setFilename(MailAttachmentTest::STR_TEST_FILENAME)
                ->setContentType(MailAttachmentTest::STR_TEST_CONTENT_TYPE)
                ->setBody(MailAttachmentTest::STR_TEST_BODY);
            $attachments[] = $attachment;
        }
        $mailer->setAttachments($attachments);

        $this->assertEquals(self::ATTR_NB_ITER, count($mailer->getAttachments()));
        $this->assertIsArray($mailer->getAttachments());
        foreach ($mailer->getAttachments() as $attachment) {
            $this->assertInstanceOf(self::ATTR_MAIL_ATTACHMENT_CLASS, $attachment);
            $this->assertEquals(MailAttachmentTest::STR_TEST_FROM_PATH, $attachment->getFromPath());
            $this->assertEquals(MailAttachmentTest::STR_TEST_FILENAME, $attachment->getFilename());
            $this->assertEquals(MailAttachmentTest::STR_TEST_CONTENT_TYPE, $attachment->getContentType());
            $this->assertEquals(MailAttachmentTest::STR_TEST_BODY, $attachment->getBody());
        }
    }

    /** **************************************************************************************************************
     *  Test subject assertion
     *  ************************************************************************************************************* */
    public function testGetSubject()
    {
        $mailer = new Mailer();
        $mailer->setSubject(self::STR_TEST_SUBJECT);
        $this->assertIsString( $mailer->getSubject());
        $this->assertEquals(self::STR_TEST_SUBJECT, $mailer->getSubject());
    }

    /** *************************************************************************************************************
     *  Test null subject assertion not accepted
     *  -------------------------------------------------------------------------------------------------------------
     *  @expectedException
     *  @test
     *  ************************************************************************************************************* */
    public function testGetSubjectNull()
    {
        $mailBody = new Mailer();
        $this->expectException(TypeError::class);
        $mailBody->setSubject(null);
    }

    /** **************************************************************************************************************
     *  Test body assertion
     *  ************************************************************************************************************* */
    public function testGetBody()
    {
        $mailer = new Mailer();
        $body = new MailBody();
        $body
            ->setHtml(MailBodyTest::STR_TEST_BODY)
            ->setTxt(MailBodyTest::STR_TEST_TXT);

        $mailer->setBody($body);
        $this->assertInstanceOf(self::ATTR_MAIL_BODY_CLASS, $mailer->getBody());
        $this->assertEquals(MailBodyTest::STR_TEST_BODY, $mailer->getBody()->getHtml());
        $this->assertEquals(MailBodyTest::STR_TEST_TXT, $mailer->getBody()->getTxt());
    }
}