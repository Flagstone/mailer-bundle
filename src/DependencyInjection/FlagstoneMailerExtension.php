<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  FlagstoneMailerBundle.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/22
 *  ***************************************************************************************************************** */

namespace Flagstone\MailerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Exception;

/** *****************************************************************************************************************
 *  Class FlagstoneMailerExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  Resources loader class
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\MailerBundle\DependencyInjection
 *  ***************************************************************************************************************** */
class FlagstoneMailerExtension extends Extension
{
    /** **************************************************************************************************************
     *  Load the resources
     *  --------------------------------------------------------------------------------------------------------------
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}