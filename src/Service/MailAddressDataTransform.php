<?php
/** *****************************************************************************************************************
 *  MailAddressDataTransform.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/25
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Service;

use Farvest\MailerBundle\Entity\MailAddress;
use Farvest\MailerBundle\Service\Exceptions\EmailNullValueException;
use Farvest\MailerBundle\Service\Exceptions\EmailValueNotSetException;

/** *****************************************************************************************************************
 *  Class MailerSender
 *  -----------------------------------------------------------------------------------------------------------------
 *  Transform array or string to MailAddress object
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Service
 *  ***************************************************************************************************************** */

class MailAddressDataTransform
{
    /** **************************************************************************************************************
     *  @param $defaultValue
     *  @param $attribute
     *  @return MailAddress
     *  @throws EmailValueNotSetException
     *  @throws EmailNullValueException
     *  ************************************************************************************************************* */
    public function transform($defaultValue, $attribute): MailAddress
    {
        $default = new MailAddress();

        if (is_array($defaultValue)) {
            if (true === array_key_exists('email', $defaultValue)) {
                $default->setEmail($defaultValue['email']);
                if (true === array_key_exists('name', $defaultValue)) {
                    $default->setName($defaultValue['name']);
                }
                return $default;
            }
            throw new EmailValueNotSetException(sprintf('Email of the %s is mandatory and is not set.', $attribute));
        } else {
            if (null !== $defaultValue) {
                $default->setEmail($defaultValue);
                return $default;
            }
            throw new EmailNullValueException(sprintf('The %s must not be null.', $attribute));
        }
    }
}