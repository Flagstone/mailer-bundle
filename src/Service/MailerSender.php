<?php
/** *****************************************************************************************************************
 *  MailerSender.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/24
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Service;

use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\MailerBundle\Entity\MailAddress;
use Farvest\MailerBundle\Entity\Mailer;
use Farvest\MailerBundle\FlagstoneMailerBundle;
use Farvest\MailerBundle\Service\Exceptions\MailDataNotSetException;
use Farvest\MailerBundle\Service\Exceptions\MailNotSentInvalidEmailException;
use Farvest\MailerBundle\Service\Exceptions\MailNotSentUnknownErrorException;
use Farvest\MailerBundle\Service\Exceptions\EmailValueNotSetException;
use Farvest\MailerBundle\Service\Exceptions\MailAttachmentErrorException;
use Swift_Mailer;
use Swift_Message;
use Exception;

/** *****************************************************************************************************************
 *  Class MailerSender
 *  -----------------------------------------------------------------------------------------------------------------
 *  Service to send beautiful and strong email
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Service
 *  ***************************************************************************************************************** */
class MailerSender
{
    /**
     *  @var MailBuilder
     */
    private $mailBuilder;
    /**
     *  @var Mailer
     */
    private $mailData;
    /**
     *  @var Swift_Mailer
     */
    private $mailer;
    /**
     * @var Logging
     */
    private $logger;
    /**
     * @var Log
     */
    private $log;

    /** **************************************************************************************************************
     *  MailerSender constructor.
     *  --------------------------------------------------------------------------------------------------------------
     *  @param Swift_Mailer $mailer
     *  @param MailBuilder $mailBuilder
     *  @param Logging $logger
     *  ************************************************************************************************************* */
    public function __construct(Swift_Mailer $mailer, MailBuilder $mailBuilder, Logging $logger)
    {
        $this->mailer = $mailer;
        $this->mailBuilder = $mailBuilder;
        $this->logger = $logger;
        $this->log = new Log();
        $this->log->setContext(FlagstoneMailerBundle::getContext());
        $this->log->setAction('Send mail');
    }

    /** **************************************************************************************************************
     *  Get mail data to send.
     *  --------------------------------------------------------------------------------------------------------------
     *  @param Mailer $mailData
     *  @return $this
     *  ************************************************************************************************************* */
    public function setData(Mailer $mailData)
    {
        $this->mailData = $mailData;
        return $this;
    }

    /** **************************************************************************************************************
     *  Send an email
     *  --------------------------------------------------------------------------------------------------------------
     *  @return bool
     *  @throws EmailValueNotSetException
     *  @throws MailAttachmentErrorException
     *  @throws MailDataNotSetException
     *  @throws MailNotSentInvalidEmailException
     *  @throws MailNotSentUnknownErrorException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function send(): bool
    {
        if (null === $this->mailData) {
            throw new MailDataNotSetException('Mail data are not set.');
        } elseif (!$this->mailData instanceof Mailer) {
            throw new MailDataNotSetException('Mail data are not a Mailer object.');
        }

        /** @var Swift_Message */
        $mail = $this->mailBuilder
            ->build($this->mailData)
            ->getMessage();

        if (false === $this->mailer->send($mail)) {
            $this->logger->error($this->log, sprintf('Email not sent.'));
            throw new MailNotSentUnknownErrorException('Email cannot be sent for an unknown reason.');
        } else {
            /** @var MailAddress */
            $to = $this->mailData->getTo();
            $this->logger->info($this->log, sprintf('Email sent to [%s <%s>].', $to->getName(), $to->getEmail() ));
            return true;
        }
    }
}
