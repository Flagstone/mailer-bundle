<?php
/** *****************************************************************************************************************
 *  MailerTemplating.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/29
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Service;

use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\MailerBundle\FlagstoneMailerBundle;
use Farvest\MailerBundle\Service\Exceptions\MailerTemplatingException;
use Farvest\TranslatorBundle\Translator\Translator;
use Farvest\TranslatorBundle\Translator\Exception\EmptyTemplateException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/** *****************************************************************************************************************
 *  Class MailerTemplating
 *  -----------------------------------------------------------------------------------------------------------------
 *  Render a email templating.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Service
 *  ***************************************************************************************************************** */
class MailerTemplating
{
    /**
     *  @var Environment
     *  ------------------------------------------------------------------------------------------------------------- */
    private $twig;
    /**
     *  @var Logging
     *  ------------------------------------------------------------------------------------------------------------- */
    private $logging;
    /**
     *  @var Translator
     *  ------------------------------------------------------------------------------------------------------------- */
    private $translator;

    /** *************************************************************************************************************
     *  MailerTemplating constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Environment $twig
     *  @param Logging $logging
     *  @param Translator $translator
     *  ************************************************************************************************************* */
    public function __construct(Environment $twig, Logging $logging, Translator $translator)
    {
        $this->twig = $twig;
        $this->logging = $logging;
        $this->translator = $translator;
    }

    /** *************************************************************************************************************
     *  Render a email tempating
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $template
     *  @param array $options
     *  @return string
     *  @throws MailerTemplatingException
     *  @throws EmptyTemplateException
     *  ************************************************************************************************************* */
    public function render(string $template, array $options): string
    {
        try {
            return $this->twig->render($template, $options);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            $log = new Log();
            $log
                ->setContext(FlagstoneMailerBundle::getContext())
                ->setAction('email rendering');

            $message = $this->translator->trans(
                'templating.error',
                [],
                FlagstoneMailerBundle::DOMAIN_TRANS
            );
            $this->logging->error($log, sprintf($message->changeLocale('en')->getMessage(), $e->getMessage()));

            throw new MailerTemplatingException(get_class($e).': '.$e->getMessage());
        }
    }
}
