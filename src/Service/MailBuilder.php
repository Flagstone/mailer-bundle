<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  MailBuilder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/10/24
 *  ***************************************************************************************************************** */

namespace Farvest\MailerBundle\Service;

use Farvest\ActionLoggingBundle\Entity\Log;
use Farvest\ActionLoggingBundle\Service\Logging;
use Farvest\MailerBundle\Entity\MailAddress;
use Farvest\MailerBundle\Entity\MailAttachment;
use Farvest\MailerBundle\Entity\Mailer;
use Farvest\MailerBundle\FlagstoneMailerBundle;
use Farvest\MailerBundle\Service\Exceptions\MailAttachmentErrorException;
use Farvest\MailerBundle\Service\Exceptions\MailNotSentInvalidEmailException;
use Swift_Message;
use Swift_Attachment;
use Swift_RfcComplianceException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use InvalidArgumentException;
use Farvest\MailerBundle\Service\Exceptions\EmailValueNotSetException;
use Farvest\MailerBundle\Service\Exceptions\SenderNotSetException;
use Symfony\Contracts\Translation\TranslatorInterface;

/** *****************************************************************************************************************
 *  Class MailConstructor
 *  -----------------------------------------------------------------------------------------------------------------
 *  Build a valid Swift Mailer mail
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\MailerBundle\Service
 *  ***************************************************************************************************************** */
class MailBuilder
{
    /**
     *  @var Swift_Message
     *  ------------------------------------------------------------------------------------------------------------- */
    protected $mail;
    /**
     *  @var Mailer
     *  ------------------------------------------------------------------------------------------------------------- */
    protected $mailData;
    /**
     *  @var ContainerBagInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $params;
    /**
     *  @var TranslatorInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private $translator;
    /**
     *  @var Logging
     *  ------------------------------------------------------------------------------------------------------------- */
    private $logger;
    /**
     *  @var Log
     *  ------------------------------------------------------------------------------------------------------------- */
    private $log;

    const TRANS_DOMAIN = 'MailerBundle';
    const ATTR_SENDER = 'sender';
    const ATTR_FROM = 'from';
    const ATTR_REPLY_TO = 'reply_to';
    const SENDER_VERIFY_ACTION = 'Verify Sender Address';
    const FROM_VERIFY_ACTION = 'Verify From Address';
    const REPLY_TO_VERIFY_ACTION = 'Verify Reply-To Address';

    /** *************************************************************************************************************
     *  MailBuilder constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param ContainerBagInterface $params
     *  @param TranslatorInterface $translator
     *  @param Logging $logger
     *  ************************************************************************************************************* */
    public function __construct(ContainerBagInterface $params, TranslatorInterface $translator, Logging $logger)
    {
        $this->mail = new Swift_Message();
        $this->params = $params;
        $this->translator = $translator;
        $this->logger = $logger;
        $this->log = new Log();
        $this->log->setContext(FlagstoneMailerBundle::getContext());
    }

    /** **************************************************************************************************************
     *  Build an email with Swift Mailer bundle
     *  --------------------------------------------------------------------------------------------------------------
     *  @param Mailer $mail
     *  @return $this
     *  @throws EmailValueNotSetException
     *  @throws MailAttachmentErrorException
     *  @throws MailNotSentInvalidEmailException
     *  ************************************************************************************************************* */
    public function build(Mailer $mail): self
    {
        $this->mailData = $mail;

        $this->verifySenderAddress();
        $this->verifyFromAddress();
        $this->verifyReplyToAddress();

        return $this
            ->setSubject()
            ->setBody()
            ->setSender()
            ->setFrom()
            ->setReplyTo()
            ->setTo()
            ->setCc()
            ->setBcc()
            ->setAttachments();
    }

    /** *************************************************************************************************************
     *  Verify the validity of the Sender email address
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws EmailValueNotSetException
     *  ************************************************************************************************************* */
    private function verifySenderAddress(): void
    {
        $this->log->setAction(self::SENDER_VERIFY_ACTION);
        if (null === $this->mailData->getSender())  {
            try {
                /**
                 *  @var MailAddress
                 */
                $defaultSenderValue = $this->params->get(self::ATTR_SENDER);
            } catch (InvalidArgumentException $e) {
                $this->logger->error($this->log, $this->translator->trans('builder.sender_not_set_error', [], self::TRANS_DOMAIN, 'en'));
                throw new SenderNotSetException($this->translator->trans('builder.sender_not_set_error', [], self::TRANS_DOMAIN));
            }
            $this->mailData->setSender((new MailAddressDataTransform())->transform($defaultSenderValue, self::ATTR_SENDER));
            $this->logger->info($this->log, sprintf('Sender address is set with default Sender address [%s].', $this->mailData->getSender()->getEmail()));
            return;
        }
        $this->logger->info($this->log, sprintf('Sender address is set by app [%s].', $this->mailData->getSender()->getEmail()));
    }

    /** *************************************************************************************************************
     *  Verify the validity of the From email address
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws EmailValueNotSetException
     *  ************************************************************************************************************* */
    private function verifyFromAddress(): void
    {
        $this->log->setAction(self::FROM_VERIFY_ACTION);
        if (null === $this->mailData->getFrom()) {
            try {
                /**
                 *  @var MailAddress
                 */
                $defaultFromValue = $this->params->get(self::ATTR_FROM);
            } catch (InvalidArgumentException $e) {
                $defaultFromValue = $this->mailData->getSender();
                $this->logger->info($this->log, sprintf('From address not set. From address is set with Sender address [%s].',$defaultFromValue->getEmail()));
            }

            if ($defaultFromValue instanceof MailAddress) {
                $this->mailData->setFrom($defaultFromValue);
                $this->logger->info($this->log, sprintf('From address is set with default From address [%s].', $this->mailData->getFrom()->getEmail()));
            } elseif (null === $defaultFromValue) {
                $this->mailData->setFrom($this->mailData->getSender());
                $this->logger->info($this->log, sprintf('From address is null. From address is set with Sender address [%s].', $this->mailData->getFrom()->getEmail()));
            } else {
                $this->mailData->setFrom((new MailAddressDataTransform())->transform($defaultFromValue, self::ATTR_FROM));
                $this->logger->info($this->log, sprintf('From address with default From address [%s].', $this->mailData->getFrom()->getEmail()));
            }
            return;
        }
        $this->logger->info($this->log, sprintf('From address is set by app [%s}.', $this->mailData->getFrom()->getEmail()));
    }

    /** *************************************************************************************************************
     *  Verify the validity of the Reply-to email address
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws EmailValueNotSetException
     *  ************************************************************************************************************* */
    private function verifyReplyToAddress(): void
    {
        $this->log->setAction(self::REPLY_TO_VERIFY_ACTION);
        if (null === $this->mailData->getReplyTo()) {
            try {
                $defaultReplyToValue = $this->params->get(self::ATTR_REPLY_TO);
            } catch (InvalidArgumentException $e) {
                $defaultReplyToValue = $this->mailData->getSender();
                $this->logger->info($this->log, 'ReplyTo address not set. ReplyTo address is set with Sender address.');
            }

            if ($defaultReplyToValue instanceof MailAddress) {
                $this->mailData->setReplyTo($defaultReplyToValue);
                $this->logger->info($this->log, sprintf('ReplyTo address is set with ReplyTo address [%s].', $this->mailData->getReplyTo()->getEmail() ));
            } elseif (null === $defaultReplyToValue) {
                $this->mailData->setReplyTo($this->mailData->getSender());
                $this->logger->info($this->log, sprintf('ReplyTo address is null. ReplyTo address is set with Sender address [%s].', $this->mailData->getReplyTo()->getEmail() ));
            } else {
                $this->mailData->setReplyTo((new MailAddressDataTransform())->transform($defaultReplyToValue, self::ATTR_REPLY_TO));
                $this->logger->info($this->log, sprintf('ReplyTo address with default ReplyTo address [%s].', $this->mailData->getReplyTo()->getEmail()));
            }
            return;
        }
        $this->logger->info($this->log, sprintf('Reply-To address is set by app [%s].',  $this->mailData->getReplyTo()->getEmail()));
    }

    /** **************************************************************************************************************
     *  Build Subject of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  ************************************************************************************************************* */
    private function setSubject(): self
    {
        $this->mail->setSubject($this->mailData->getSubject());
        return $this;
    }

    /** **************************************************************************************************************
     *  Build Body of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  ************************************************************************************************************* */
    private function setBody(): self
    {
        $this->mail->setBody($this->mailData->getBody()->getHtml(), 'text/html');
        if (null !== $this->mailData->getBody()->getTxt()) {
            $this->mail->addPart($this->mailData->getBody()->getTxt(), 'text/plain');
        }
        return $this;
    }

    /** **************************************************************************************************************
     *  Build Sender part part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  @throws MailNotSentInvalidEmailException
     *  ************************************************************************************************************* */
    private function setSender(): self
    {
        try {
            if (null === $this->mailData->getSender()->getName()) {
                $this->mail->setSender($this->mailData->getSender()->getEmail());
            } else {
                $this->mail->setSender(
                    $this->mailData->getSender()->getEmail(),
                    $this->mailData->getSender()->getName()
                );
            }
            return $this;
        } catch (Swift_RfcComplianceException $e) {
            throw new MailNotSentInvalidEmailException(
                $this->formatMailAddressError($this->mailData->getSender(), 'Sender')
            );
        }
    }

    /** **************************************************************************************************************
     *  Build From part part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @throws MailNotSentInvalidEmailException
     *  @return $this
     *  ************************************************************************************************************* */
    private function setFrom(): self
    {
        try {
            if (null === $this->mailData->getFrom()->getName()) {
                $this->mail->setFrom($this->mailData->getFrom()->getEmail());
            } else {
                $this->mail->setFrom(
                    $this->mailData->getFrom()->getEmail(),
                    $this->mailData->getFrom()->getName()
                );
            }
            return $this;
        } catch (Swift_RfcComplianceException $e) {
            throw new MailNotSentInvalidEmailException(
                $this->formatMailAddressError($this->mailData->getSender(), 'From')
            );
        }
    }

    /** **************************************************************************************************************
     *  Build To part part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @throws MailNotSentInvalidEmailException
     *  @return $this
     *  ************************************************************************************************************* */
    private function setTo(): self
    {
        try {
            if (null === $this->mailData->getTo()->getName()) {
                $this->mail->setTo($this->mailData->getTo()->getEmail());
            } else {
                $this->mail->setTo(
                    $this->mailData->getTo()->getEmail(),
                    $this->mailData->getTo()->getName()
                );
            }
            return $this;
        } catch (Swift_RfcComplianceException $e) {
            throw new MailNotSentInvalidEmailException(
                $this->formatMailAddressError($this->mailData->getSender(), 'To')
            );
        }
    }

    /** **************************************************************************************************************
     *  Build replyTo part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  ************************************************************************************************************* */
    private function setReplyTo(): self
    {
        if (null !== $this->mailData->getReplyTo()) {
            if (null === $this->mailData->getReplyTo()->getName()) {
                $this->mail->setReplyTo($this->mailData->getReplyTo()->getEmail());
            } else {
                $this->mail->setReplyTo(
                    $this->mailData->getReplyTo()->getEmail(),
                    $this->mailData->getReplyTo()->getName()
                );
            }
        }
        return $this;
    }

    /** **************************************************************************************************************
     *  Build Carbon-Copy (cc) part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  ************************************************************************************************************* */
    private function setCc(): self
    {
        /** @var MailAddress $cc */
        foreach ($this->mailData->getCc() as $cc) {
            if (null === $cc->getName()) {
                $this->mail->addCc($cc->getEmail());
            } else {
                $this->mail->addCc(
                    $cc->getEmail(),
                    $cc->getName()
                );
            }
        }
        return $this;
    }

    /** **************************************************************************************************************
     *  Build Blind-Carbon-Copy (bcc) part of an email.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  ************************************************************************************************************* */
    private function setBcc(): self
    {
        /** @var MailAddress $bcc */
        foreach ($this->mailData->getBcc() as $bcc) {
            if (null === $bcc->getName()) {
                $this->mail->addBcc($bcc->getEmail());
            } else {
                $this->mail->addBcc(
                    $bcc->getEmail(),
                    $bcc->getName()
                );
            }
        }
        return $this;
    }

    /** **************************************************************************************************************
     *  Build an attachement.
     *  --------------------------------------------------------------------------------------------------------------
     *  @return $this
     *  @throws MailAttachmentErrorException
     *  ************************************************************************************************************* */
    private function setAttachments(): self
    {
        if (null !== $this->mailData->getAttachments()) {
            /** @var MailAttachment $attach */
            foreach ($this->mailData->getAttachments() as $attach) {
                if (null === $attach->getBody()) {
                    $this->mail->attach($this->addStaticAttachment($attach));
                } else {
                    $this->mail->attach($this->addDynamicAttachment($attach));
                }
            }
        }

        return $this;
    }

    /** **************************************************************************************************************
     *  Build an attachment for a dynamic file.
     *  --------------------------------------------------------------------------------------------------------------
     *  @param MailAttachment $attach
     *  @return Swift_Attachment
     *  @throws MailAttachmentErrorException
     *  ************************************************************************************************************* */
    private function addDynamicAttachment(MailAttachment $attach): Swift_Attachment
    {
        $body = $attach->getBody();
        $filename = $attach->getFilename();
        $contentType = $attach->getContentType();

        if (null !== $body && null !== $filename && null !== $contentType) {
            $attachment = new Swift_Attachment();
            $attachment
                ->setFilename($filename)
                ->setContentType($contentType)
                ->setBody($body);
            return $attachment;
        }

        throw new MailAttachmentErrorException($this->translator->trans('builder.invalid_attachment_dynamic', [], self::TRANS_DOMAIN));
    }

    /** **************************************************************************************************************
     *  Build an attachment for a static file.
     *  --------------------------------------------------------------------------------------------------------------
     *  @param MailAttachment $attach
     *  @return Swift_Attachment
     *  @throws MailAttachmentErrorException
     *  ************************************************************************************************************* */
    private function addStaticAttachment(MailAttachment $attach)
    {
        $path = $attach->getFromPath();
        $filename = $attach->getFilename();
        $contentType = $attach->getContentType();

        if (null !== $filename) {
            $attachment = Swift_Attachment::fromPath($path)->setFilename($filename)->setContentType($contentType);
            return $attachment;
        }

        throw new MailAttachmentErrorException($this->translator->trans('builder.invalid_attachment_static', [], self::TRANS_DOMAIN));
    }

    /** *************************************************************************************************************
     *  Return the Swift Message object
     *  -------------------------------------------------------------------------------------------------------------
     *  @return Swift_Message
     *  ************************************************************************************************************* */
    public function getMessage(): Swift_Message
    {
        return $this->mail;
    }

    /** *************************************************************************************************************
     *  Format the error when an email address has not the good format and return the translated string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param MailAddress $address
     *  @param string $attribute
     *  @param string|null $locale
     *  @return string
     *  ************************************************************************************************************* */
    private function formatMailAddressError(MailAddress $address, string $attribute, ?string $locale = null): string
    {
        if (null === $locale) {
            $translatedMessage = $this->translator->trans('builder.invalid_email_error', [], self::TRANS_DOMAIN);
        } else {
            $translatedMessage = $this->translator->trans('builder.invalid_email_error', [], self::TRANS_DOMAIN, $locale);
        }
        return sprintf(
            $translatedMessage,
            $address->getEmail(),
            $attribute
        );
    }
}