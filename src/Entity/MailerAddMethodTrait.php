<?php
/** *****************************************************************************************************************
 *  MailerAddMethodTrait.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Creation Date : 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\MailerBundle\Entity;

/** *****************************************************************************************************************
 *  Class MailerAddMethodTrait
 *  -----------------------------------------------------------------------------------------------------------------
 *  Trait to split Mailer class
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\MailerBundle\Entity
 *  ***************************************************************************************************************** */
trait MailerAddMethodTrait
{
    /** *************************************************************************************************************
     *  Add a blind Carbon copy
     *  -------------------------------------------------------------------------------------------------------------
     *  @param MailAddress $bcc
     *  @return $this
     *  ************************************************************************************************************* */
    public function addBcc(MailAddress $bcc): self
    {
        $this->bcc[] = $bcc;
        return $this;
    }

    /** *************************************************************************************************************
     *  Add a Carbon copy
     *  -------------------------------------------------------------------------------------------------------------
     *  @param MailAddress $cc
     *  @return $this
     *  ************************************************************************************************************* */
    public function addCc(MailAddress $cc): self
    {
        $this->cc[] = $cc;
        return $this;
    }

    /** *************************************************************************************************************
     *  Add an attachment on Mailer
     *  -------------------------------------------------------------------------------------------------------------
     *  @param MailAttachment $attachment
     *  @return $this
     *  ************************************************************************************************************* */
    public function addAttachment(MailAttachment $attachment): self
    {
        $this->attachments[] = $attachment;
        return $this;
    }
}